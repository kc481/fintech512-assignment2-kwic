package src.main.java;

import java.io.*;
import java.util.*;

public class KWIC {
    public static void main(String[] args) throws IOException {
        // 使用 System.in 创建 BufferedReader
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = "";
        String[] ignorewords = new String[15];
        int ignorewords_number = 0;
        while (!str .equals("::")){
            str = br.readLine();
            ignorewords[ignorewords_number] = str;
            ignorewords_number++;
        };
        String[] text = new String[200];
        int text_number = 0;
        while (!str.equals("")){
            str = br.readLine();
            text[text_number] = str;
            text_number++;
        };
        for(int j = 0; j < ignorewords_number; j++) {
            ignorewords[j].toLowerCase();
        }
        for(int j = 0; j < text_number; j++) {
            String[] arr = text[j].split(" ");
            for (int k = 0; k < arr.length; k++) {
                arr[k].toLowerCase();
            }
        }
        int output_index = 0;
        String[] output = new String[500];
        for(int i = 97; i < 123; i++) {
            for(int j = 0; j < text_number; j++) {
                String[] arr = text[j].split(" ");
                for(int k = 0; k < arr.length; k++) {
                    int symbol = 0;
                    for(int l = 0; l < ignorewords_number; l++) {
                        if(arr[k] == ignorewords[l]){
                           symbol = 1;
                        }
                    }
                    if(((int)(arr[k].charAt(0)) == i) && (symbol == 0)) {
                        text[j].toLowerCase();
                        arr[k].toUpperCase();
                        output[output_index] = text[j];
                        output_index++;
                    }
                }
            }
        }
        for(int i = 0; i < output_index; i++) {
            System.out.print(output[i]);
            System.out.print('\n');
        }
    }
}
